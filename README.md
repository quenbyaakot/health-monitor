# CastleWars helth-monitor
CastleWars component. Monitors target machines and broadcasts current state to the players.

For installation instructions and general information on the project please refer to the [main repository](https://gitlab.com/quenbyaakot/main-server). 
