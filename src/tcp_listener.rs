use futures::future::{select, Either};
use quenbyaakot_core::types::{ChallengeConfig, State};
use std::{
    collections::HashMap,
    net::{IpAddr, Shutdown, SocketAddr},
    sync::{Arc, Mutex},
    time::{Duration, SystemTime},
};
use tokio::{
    codec::{BytesCodec, Decoder},
    net::TcpListener,
    prelude::*,
    sync::{mpsc, oneshot},
    timer::delay_for,
};

pub async fn start(
    config: ChallengeConfig,
    addr: SocketAddr,
    mut monitor_tx: mpsc::Sender<String>,
) {
    let mut machine_states = HashMap::with_capacity(config.machines.len());

    config.machines.iter().for_each(|machine| {
        machine_states.insert(machine.address, State::Unavailable);
    });

    let machine_states = Arc::new(Mutex::new(machine_states));

    let attacked_machines: HashMap<IpAddr, oneshot::Sender<()>> = HashMap::new();
    let attacked_machines = Arc::new(Mutex::new(attacked_machines));

    let json = serde_json::to_string(&*machine_states.lock().unwrap())
        .expect("error: cannot serialize state");
    monitor_tx
        .send(json)
        .await
        .expect("error: cannot broadcast state");

    let mut listener = TcpListener::bind(addr)
        .await
        .expect("error: cannot start TCP listener");

    println!("info: TCP listener started");

    let max_revival_time = config.max_revival_time;

    loop {
        let socket = listener.accept().await;
        let states = Arc::clone(&machine_states);
        let mut tx = monitor_tx.clone();
        let attacked = Arc::clone(&attacked_machines);

        if let Ok((socket, _)) = socket {
            tokio::spawn(async move {
                let ip = socket.peer_addr().unwrap().ip();
                println!("info: new incoming connection with ip: {}", ip);

                if !states.lock().unwrap().contains_key(&ip) {
                    socket
                        .shutdown(Shutdown::Both)
                        .expect("error: cannot close socket");

                    println!("info: incoming connection closed with reason `not allowed`");
                    return;
                }

                if let Some(sender) = attacked.lock().unwrap().remove(&ip) {
                    sender
                        .send(())
                        .expect("error: receiver unexpectedly dropped");
                }

                let state = *states.lock().unwrap().get(&ip).unwrap();

                match state {
                    State::Connected(_) => {
                        socket
                            .shutdown(Shutdown::Both)
                            .expect("error: cannot close socket");

                        println!(
                            "info: incoming connection closed with reason `already connected`"
                        );
                        return;
                    }
                    State::Disconnected(_) | State::Unavailable => {
                        states.lock().unwrap().insert(ip, State::Connected(SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis()));

                        let json = serde_json::to_string(&*states.lock().unwrap())
                            .expect("error: cannot serialize state");
                        tx.send(json).await.expect("error: cannot broadcast state");
                    }
                    State::Hacked(_) => {
                        socket
                            .shutdown(Shutdown::Both)
                            .expect("error: cannot close socket");

                        println!("info: incoming connection closed with reason `machine hacked`");
                        return;
                    }
                }

                println!("info: machine connected. ip: {}", ip);

                let mut framed = BytesCodec::new().framed(socket);

                while let Some(message) = framed.next().await {
                    match message {
                        Ok(_) => eprintln!("error: recieved some unexpected bytes"),
                        Err(_) => {
                            states.lock().unwrap().insert(ip, State::Disconnected(SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis()));

                            let json = serde_json::to_string(&*states.lock().unwrap())
                                .expect("error: cannot serialize state");
                            tx.send(json).await.expect("error: cannot broadcast state");

                            println!("info: machine disconnected. ip: {}", ip);

                            let (sender, receiver) = oneshot::channel::<()>();
                            attacked.lock().unwrap().insert(ip, sender);

                            let delay = delay_for(Duration::from_millis(max_revival_time));

                            match select(receiver, delay).await {
                                Either::Left(result) => match result.0 {
                                    Err(_) => eprintln!("error: sender unexpectedly dropped"),
                                    Ok(()) => println!("info: machine {} succesfully defended", ip),
                                },
                                Either::Right(_) => {
                                    println!("info: machine {} succesfully hacked", ip);
                                    attacked.lock().unwrap().remove(&ip);
                                    states.lock().unwrap().insert(ip, State::Hacked(SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis()));

                                    let json = serde_json::to_string(&*states.lock().unwrap())
                                        .expect("error: cannot serialize state");
                                    tx.send(json).await.expect("error: cannot broadcast state");
                                }
                            };

                            return;
                        }
                    }
                }
            });
        }
    }
}
