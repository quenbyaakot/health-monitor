mod tcp_listener;

use quenbyaakot_core::types::ChallengeConfig;
use tcp_listener::start;
use tokio::{
    net::TcpListener,
    sync::{mpsc, watch},
};
use tokio_tungstenite::accept_async;
use tungstenite::Message;

pub async fn start_server(config: ChallengeConfig) {
    let tcp_addr = config.tcp_addr;
    let ws_addr = config.ws_addr;

    let (monitor_tx, mut monitor_rx) = mpsc::channel(100);
    let (websocket_tx, websocket_rx) = watch::channel(String::new());

    tokio::spawn(async move {
        while let Some(state) = monitor_rx.recv().await {
            websocket_tx
                .broadcast(state)
                .expect("error: cannot broadcast state");
        }
    });

    tokio::spawn(async move {
        start(config, tcp_addr, monitor_tx).await;
    });

    let mut listener = TcpListener::bind(ws_addr)
        .await
        .expect("error: cannot start WS server");

    println!("info: WS server started");

    loop {
        let socket = listener.accept().await;

        if let Ok((socket, _)) = socket {
            if let Ok(mut ws_stream) = accept_async(socket).await {
                println!("info: new websocket connection");
                let mut rx = websocket_rx.clone();

                tokio::spawn(async move {
                    while let Some(state) = rx.recv().await {
                        ws_stream
                            .send(Message::text(state))
                            .await
                            .expect("error: failed to send state");
                    }
                });
            } else {
                eprintln!("error: cannot establish websocket handshake");
            }
        }
    }
}
