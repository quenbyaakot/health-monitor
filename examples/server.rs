use health_monitor::start_server;
use quenbyaakot_core::types::ChallengeConfig;
use std::{env, fs::read_to_string};

#[tokio::main]
async fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        eprintln!("error: missing config path argument");
        return;
    }

    let config = read_to_string(&args[1]).expect("error: cannot read config");
    let config: ChallengeConfig =
        serde_yaml::from_str(&config).expect("error: invalid config file");

    start_server(config).await;
}
